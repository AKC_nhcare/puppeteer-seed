FROM node:12

RUN apt-get -qq update

# install chrome
RUN apt-get -y install lsb-release libappindicator3-1
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb || true
RUN apt-get -fy install

WORKDIR /dockerized-cures
COPY package*.json ./
RUN npm install
COPY . .

CMD [ "node", "index.js" ]