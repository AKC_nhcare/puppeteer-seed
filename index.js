const puppeteer = require("puppeteer");
const dotenv = require("dotenv");
dotenv.config();


(async () => {
      const browser = await puppeteer.launch({ headless:true,  args: ["--no-sandbox"],});
      const page = await browser.newPage();

      console.log(process.env.DESTINATION_WEBPAGE);

      function getText(linkText) {
            linkText = linkText.replace(/\r\n|\r/g, "\n");
            linkText = linkText.replace(/\ +/g, " ");

            // Replace &nbsp; with a space
            var nbspPattern = new RegExp(String.fromCharCode(160), "g");
            return linkText.replace(nbspPattern, " ");
      }

      async function login() {
            await page.goto(process.env.DESTINATION_WEBPAGE);
            console.log("navigated to login form");

            await page.type("#username", process.env.USER);
            await page.type("#password", process.env.PWD);
            console.log("entered credentials");

            await page.evaluate(() => {
                  document .querySelector('input[type="submit"]')
                        .click();
            });
            console.log("clicked submit");

           // await page.click('input[type="submit"]');
           
            await page.waitForSelector("#homeForm");
      }

      async function getReportLink() {
            
    
            const anchors = await page.$$("a");
            const linkString = "Patient Activity Report";
            for (var i = 0; i < anchors.length; i++) {
                  let valueHandle = await anchors[i].getProperty("innerText");
                  let linkText = await valueHandle.jsonValue();
                  const text = getText(linkText);
                  if (linkString == text) {
                        console.log("Found", linkString);
                        const reportLink = anchors[i];
                        return reportLink
                  }
            }

           
      }



      async function enterPatientInformation() {
            const lnameInput = await page.$(
                  `input[name="patientSrchForm\:parTabs\:lastName"]`
            );

            await page.type(
                  `input[name="patientSrchForm\:parTabs\:lastName"]`,
                  process.env.PATIENT_LAST_NAME
            );
            await page.type(
                  `input[name="patientSrchForm\:parTabs\:firstName"]`,
                  process.env.PATIENT_FIRST_NAME
            );
// need to get focus first
          
            await page.click(`input[name="patientSrchForm\:parTabs\:dob_input"]`);

            await page.type(
                  `input[name="patientSrchForm\:parTabs\:dob_input"]`,
                  "03251969",
                  { delay: 100 }
            );

            await page.evaluate(() => {
                  document .querySelector( `button[name="patientSrchForm\:parTabs\:searchBtn"]`)
                        .click();
            });


            console.log("entered patient info and clicked search");
      }

      async function getSearchResults() {
            let results = "";

            await page.waitForSelector(
                  `input[name="patientSrchForm\:parTabs\:patientTbl_checkbox"]`
            );

            console.log("found select all button");

            await page.evaluate(() => {
                  document
                        .querySelector(`input[name="patientSrchForm\:parTabs\:patientTbl_checkbox"]` )
                        .click();
            });

            console.log("clicked select all results");
      }

      async function generateReport() {
            await page.waitForSelector(
                  `button[name="patientSrchForm\:parTabs\:patientTbl\:genReportBtn"]`,
                  { visible: true }
            );

            console.log("found generate report button");

            await page.evaluate(() => {
                  document
                        .querySelector(
                              `button[name="patientSrchForm\:parTabs\:patientTbl\:genReportBtn"]`
                        )
                        .click();
            });

            console.log("clicked generate report button");
      }

      async function downloadPdf() {

            await page.waitForSelector(
                  `button[name="patientSrchForm\:parTabs\:printConBtn"]`,
                  { visible: true }
            );

            console.log("download pdf visible");

            await page.evaluate(() => {
                  document
                        .querySelector(
                              `button[name="patientSrchForm\:parTabs\:printConBtn"]`
                        )
                        .click();
            });

            console.log("clicked download pdf");
      }

      try {
             console.log("hello docker");

             await login();
       
            const reportLink = await getReportLink();

             await reportLink.click();

             console.log("report link clcikc")
 
             await page.waitForSelector("#patientSrchForm");

             await enterPatientInformation();

             await getSearchResults();

             await generateReport();

             await downloadPdf();
      } catch (error) {
            console.log(error);
      }
})();



     // await page.screenshot({ path: "dashboard.png", fullPage: true });